MAKEFLAGS+=--no-builtin-rules


# CC = clang
# CC = gcc

# CXX = clang++
CXX = g++

ifeq ($(CXX),clang++)
	LIBS += -stdlib=libc++
	CXX_FLAGS += -stdlib=libc++ 
else

endif




CXX_FLAGS += -std=gnu++11 -Wall -Wextra -pedantic -O3
CXX_FLAGS += -pg -g
LIBS += -pg -g

# we need to do some of those, the warning is annoying 
# CXX_FLAGS += -Wno-pointer-arith

LIBS += -lSDL2 
LIBS += -lGLEW 
# LIBS += -lGLU
LIBS += -lGL
# LIBS += -pthread
# LIBS += -lboost_system 
LIBS += -std=gnu++11

# C_FLAGS += -W -ansi -static

SOURCES = $(wildcard src/*/*.cc)
OBJECTS = $(SOURCES:%.cc=%.o)

MAINS_SOURCES = $(wildcard src/*.cc)
MAINS_OBJECTS   = $(MAINS_SOURCES:%.cc=%.o)

PROGRAM_NAME = $(MAINS_SOURCES:src/%.cc=%)


# all: $(MAINS_SOURCES) $(OBJECTS) $(PROGRAM_NAME) 
# $(PROGRAM_NAME): %: src/%.o
# 	$(CXX) $(LIBS) $(OBJECTS) src/$*.o -o $@

all:$(PROGRAM_NAME)
$(OBJECTS) : %.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(PROGRAM_NAME): %: src/%.o $(OBJECTS)
	$(CXX) $(LIBS) $^ -o $@

# all: $(PROGRAM_NAME) 
# $(PROGRAM_NAME):$(OBJECTS) $(MAINS_OBJECTS)
# 	# $(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^
#	$(CXX) $(LIBS) $(OBJECTS) $(MAINS_OBJECTS) -o $@




# -include $(OBJS:.o=.d)

%.o: %.cc
	$(CXX) -c $(CXX_FLAGS) $*.cc -o $*.o
#	$(CXX) -MM $(CXX_FLAGS) $*.cc > $*.d

run:all
	# ./$(PROGRAM_NAME) data/rmesures.txt

run-software:all
	# LIBGL_DEBUG=verbose LIBGL_SHOW_FPS=1 LIBGL_ALWAYS_SOFTWARE=1 ./$(PROGRAM_NAME)

run-profiling:all
	# LIBGL_DEBUG=verbose LIBGL_SHOW_FPS=1 ./$(PROGRAM_NAME) ;gprof $(PROGRAM_NAME) gmon.out

clean:
	rm -f $(OBJECTS) $(MAINS_OBJECTS)

mrproper:
	rm -f $(PROGRAM_NAME) $(OBJECTS) $(MAINS_OBJECTS)
	rm -f $(MAINS_OBJECTS)


