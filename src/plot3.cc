#include "compilation_option.h"

#include "glouglou/glouglou.h"
#include "glouglou_helper/glouglou_geometry.h"

#include "laurent_eyer_data/data_parser.h"
#include "laurent_eyer_data/data_treatment.h"
#include <algorithm>
// #include <map>

#include "c+++/c+++.h"
// #include <vector>
// #include <string>
// #include <unistd.h>

using namespace std;
using namespace glm;

float dither_noise(float range) {
	return range - 2*range * (static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
}

int main(int argc, char* argv[]) {
	string file_path;
	if (argc==1) {
		cout<<"usage : "<< argv[0] <<" [path_to_file]"<<endl;
		return -1;
	} else {
		file_path = argv[1];
	}

	// -------------------
	// step 1 : load data

	vector<Data> datas;
	get_data_from_file_from(file_path,datas,3);
	// get_data_from_file_range(file_path,datas,3,100000);

	// -------------------
	// step 2 : sort data

	// it's already sorded by id
	// sort(datas.begin(),datas.end(),sort_call_back_data_by_id);

	// this is useless for the representation I'm trying now
	// sort(datas.begin(),datas.end(),sort_call_back_data_by_night);

	// -------------------
	// step 3 : split data by ID (make a vector for each star)
	vector<vector<Data>> datas_split_by_id;
	vector<Data> tmp = vector<Data>(0);
	int datas_code = datas[0].CODE;
	cout<<"hello"<<flush;
	for (size_t i = 0; i < datas.size(); i++) {
		cout<<"."<<flush;
		if(datas[i].CODE == datas_code){
			tmp.push_back(datas[i]);
		} else {
			datas_code = datas[i].CODE;
			datas_split_by_id.push_back(tmp);
			tmp = vector<Data>({datas[i]});
		}
	}

	// sort data by data aquisition
	sort(datas_split_by_id.begin(),datas_split_by_id.end(),sort_call_back_vector_by_size<Data>);

	// for(size_t i=0;i<datas_split_by_id.size();i++){
	// 	cout<< datas_split_by_id[i].size()<<endl<<flush;
	// }
	// cout << datas_split_by_id[0].size() << endl << flush;
	// cout << datas_split_by_id[1].size() << endl << flush;

	// cout<< datas_split_by_id[0].size()<<endl<<flush;
	// cout<< datas_split_by_id[datas_split_by_id.size()-1].size()<<endl<<flush;



//========================================================
//= STEP 0 : CREATE A CONTEXT ============================
//========================================================

	Glouglou::Context ctx = Glouglou::Context(vec2(400,300),3,3);

//========================================================
//= SETP 1 : MAKE A PROGRAM ON THE GPU ===================
//========================================================

	glDisable(GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// glBlendFunc(GL_ZERO, GL_SRC_COLOR);
	// glBlendFunc(GL_ONE, GL_ONE);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	Glouglou::GPU_Program shader_dots;
		// shader.set_vertex_shader( file_to_str("shader/identity.vert"));
		// shader.set_fragment_shader( file_to_str("shader/noise.frag"));
		shader_dots.set_vertex_shader( file_to_str("shader/laurent_data_zoom.vert"));
		shader_dots.set_fragment_shader( file_to_str("shader/color.frag"));
		// shader_dots.set_geometry_shader( file_to_str("shader/laurent_eyer_points.geom"));
		shader_dots.compile();

	Glouglou::GPU_Program shader_square;
		// shader.set_vertex_shader( file_to_str("shader/identity.vert"));
		// shader.set_fragment_shader( file_to_str("shader/noise.frag"));
		shader_square.set_vertex_shader( file_to_str("shader/laurent_data_zoom.vert"));
		shader_square.set_fragment_shader( file_to_str("shader/color.frag"));
		shader_square.set_geometry_shader( file_to_str("shader/laurent_eyer_points.geom"));
		shader_square.compile();


//========================================================
//= STEP 2 : SEND DATA TO THE GPU ========================
//========================================================
	// vector<int>		 laurent_data_vector_CODE;
	// vector<int>		 laurent_data_vector_NUIT;
	// vector<float>	 laurent_data_vector_PJ;
	// vector<float>	 laurent_data_vector_FZ;
	// vector<int>		 laurent_data_vector_IPV;
	// vector<float>	 laurent_data_vector_VM;
	// vector<int>		 laurent_data_vector_IPC;
	vector<float>	 laurent_data_vector_U;
	vector<float>	 laurent_data_vector_V;
	// vector<float>	 laurent_data_vector_B1;
	// vector<float>	 laurent_data_vector_B2;
	// vector<float>	 laurent_data_vector_V1;
	// vector<float>	 laurent_data_vector_G;
	vector<unsigned int> indices;

	size_t index_counter_last_batch=0;
	size_t index_counter=0;
	for (size_t i=0;i<datas_split_by_id.size();i++) {
		vector<Data>& d = datas_split_by_id[i];
		for (size_t i = 0; i < d.size(); i++) {
			// enlever si IPV ou IPC == 0 1 ou 2
			if (d[i].IPV == 0 || d[i].IPV == 1 || d[i].IPV == 2 ||
				d[i].IPC == 0 || d[i].IPC == 1 || d[i].IPC == 2) {
				// laurent_data_vector_CODE.push_back( datas[i].CODE );
				// laurent_data_vector_NUIT.push_back( datas[i].NUIT );
				// laurent_data_vector_PJ.push_back(   datas[i].PJ   );
				// laurent_data_vector_FZ.push_back(   datas[i].FZ   );
				// laurent_data_vector_IPV.push_back(  datas[i].IPV  );
				// laurent_data_vector_VM.push_back(   datas[i].VM   );
				// laurent_data_vector_IPC.push_back(  datas[i].IPC  );
				laurent_data_vector_U.push_back( d[i].U + dither_noise(0.0005) );
				laurent_data_vector_V.push_back( d[i].V + dither_noise(0.0005) );
				// laurent_data_vector_B1.push_back(   datas[i].B1   );
				// laurent_data_vector_B2.push_back(   datas[i].B2   );
				// laurent_data_vector_V1.push_back(   datas[i].V1   );
				// laurent_data_vector_G.push_back(    datas[i].G    );
				index_counter++;
			}
		}
		size_t i_max = index_counter - index_counter_last_batch;
		if( i_max>1 ) {
			for(size_t i=0;i < i_max - 1 ;i++) {
				indices.push_back( index_counter_last_batch + i );
				indices.push_back( index_counter_last_batch + i + 1 );
			}
		}
		index_counter_last_batch = index_counter;
	}

	Glouglou::GPU_Data laurent_data_X;
	Glouglou::GPU_Data laurent_data_Y;

	laurent_data_X.to_VRAM(laurent_data_vector_U,indices);
	laurent_data_Y.to_VRAM(laurent_data_vector_V,indices);
	// Glouglou::GPU_Data::to_VRAM(unsigned int data_dimension, const std::vector<float>& vertices, const std::vector<unsigned int>& indices)

//========================================================
//= STEP 3 : SETUP PROGRAM CALL (LINK DATA TO GPU INPUT) =
//========================================================

	Glouglou::GPU_Program_Runner program_runner_dots(shader_dots);
		// program_runner.link_data(square_mesh,"position");
		// program_runner.link_data(square_temperature,"temperature");

		program_runner_dots.link_data(laurent_data_X,"X");
		program_runner_dots.link_data(laurent_data_Y,"Y");

//========================================================
//========================================================
//========================================================











	vec2 zoom;
	zoom.x=1;
	zoom.y=1;
	vec2 camera_position;
	camera_position.x=0;
	camera_position.y=0;

	bool quit=false;
	SDL_Event e;
	const Uint8 *keyboard_state;
	float t=0;
	float exposure =.2;
	while(!quit) {
		keyboard_state = SDL_GetKeyboardState(NULL);
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			if (e.type == SDL_MOUSEWHEEL) {
				if(keyboard_state[SDL_SCANCODE_LCTRL] ) {
					zoom.x *= pow(1.1,-e.wheel.x);
					zoom.y *= pow(1.1,e.wheel.y);
				} else if (keyboard_state[SDL_SCANCODE_LSHIFT]) {
					exposure += 0.01 * e.wheel.y;
					exposure = clamp(exposure , 0.01f , 1.f);
				}else {
					camera_position.x +=-0.02*e.wheel.x/zoom.x;
					camera_position.y += 0.02*e.wheel.y/zoom.y;
				}
			}
			if (e.type == SDL_WINDOWEVENT) {
				if(e.window.event == SDL_WINDOWEVENT_RESIZED) {
					glViewport(0,0,e.window.data1,e.window.data2);
				}
			}
			if (e.type == SDL_KEYDOWN) {
				if (e.key.keysym.sym == SDLK_p) {

				}
			}
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// DRAW
		t+=0.001;

		{
			shader_dots.use();
			// shader.set_uniform("time",t);
			shader_dots.set_uniform("color",vec4(1.,1.,1.,exposure));
			shader_dots.set_uniform("zoom_factor",zoom);
			shader_dots.set_uniform("camera_position",camera_position);
			// shader_dots.set_uniform("geom_square_size",0.0005f);


			// program_runner_dots.draw_triangles();
			program_runner_dots.draw_edges();
			// program_runner_dots.draw_points();
		}

		ctx.swap();
	}
	return 0;
}
