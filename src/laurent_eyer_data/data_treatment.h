#pragma once

#include <vector>
#include "data_parser.h"

template<typename T>
bool sort_call_back_vector_by_size (const std::vector<T>& v1,const std::vector<T>& v2) {
	return v1.size() < v2.size();
}

bool sort_call_back_data_by_night (const Data& d1,const Data& d2); 
bool sort_call_back_data_by_id (const Data& d1,const Data& d2);

