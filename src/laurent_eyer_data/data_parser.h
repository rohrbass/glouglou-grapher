#pragma once

#include <limits>
#include <sstream>
#include <fstream>
#include <vector>

struct Data {
	int CODE;
	int NUIT;
	float PJ;
	float FZ;
	int IPV;
	float VM;
	int IPC;
	float U;
	float V;
	float B1;
	float B2;
	float V1;
	float G;
};

#define STR_TO_DATA_SUCCESS 0
#define STR_TO_DATA_FAILURE 1
#define STR_TO_DATA_INCOMPLETE 2

// reads str and put the parsed struct in d
int str_to_data(std::string str,Data& d);

// returns 0 = failed
// returns 1 = success
// file_path is the path to the file that will be parsed
// datas is where the function outputs
// beginning is the number of the line you want to begin at [1,2,3...].
//    if set to 0, it does the same thing as a 1
// count is the number of line parsed (1 will parse one line) 
int get_data_from_file(std::string file_path,std::vector<Data>& datas);
int get_data_from_file_from(std::string file_path,std::vector<Data>& datas, unsigned int beginning);
int get_data_from_file_range(std::string file_path,std::vector<Data>& datas, unsigned int beginning, unsigned  int count);
int get_data_from_file_until(std::string file_path,std::vector<Data>& datas, unsigned  int count);


