#include "../compilation_option.h"
#include "data_parser.h"

#include <iostream>

int str_to_data(std::string str,Data& d) {
	std::stringstream str_stream(str);
	std::string seg;
	int index=0;
	int ret = STR_TO_DATA_SUCCESS;
	while(getline(str_stream, seg, '\t')) {
		if (seg == "") {
			switch(index) {
				case 0: d.CODE = std::numeric_limits<int>::quiet_NaN();
				case 1: d.NUIT = std::numeric_limits<int>::quiet_NaN();
				case 2: d.PJ   = std::numeric_limits<float>::quiet_NaN();
				case 3: d.FZ   = std::numeric_limits<float>::quiet_NaN();
				case 4: d.IPV  = std::numeric_limits<int>::quiet_NaN();
				case 5: d.VM   = std::numeric_limits<float>::quiet_NaN();
				case 6: d.IPC  = std::numeric_limits<int>::quiet_NaN();
				case 7: d.U    = std::numeric_limits<float>::quiet_NaN();
				case 8: d.V    = std::numeric_limits<float>::quiet_NaN();
				case 9: d.B1   = std::numeric_limits<float>::quiet_NaN();
				case 10: d.B2  = std::numeric_limits<float>::quiet_NaN();
				case 11: d.V1  = std::numeric_limits<float>::quiet_NaN();
				case 12: d.G   = std::numeric_limits<float>::quiet_NaN();
			}
			ret = STR_TO_DATA_INCOMPLETE;
		} else {
			try {
				switch(index) {
					case 0: d.CODE = stoi(seg);
					case 1: d.NUIT = stoi(seg);
					case 2: d.PJ   = stof(seg);
					case 3: d.FZ   = stof(seg);
					case 4: d.IPV  = stoi(seg);
					case 5: d.VM   = stof(seg);
					case 6: d.IPC  = stoi(seg);
					case 7: d.U    = stof(seg);
					case 8: d.V    = stof(seg);
					case 9: d.B1   = stof(seg);
					case 10: d.B2  = stof(seg);
					case 11: d.V1  = stof(seg);
					case 12: d.G   = stof(seg);
				}
			} catch (const std::exception& e) {
				return STR_TO_DATA_FAILURE;
			}
		}
		index++;
	}
	return ret;
}




int get_data_from_file(std::string file_path,std::vector<Data>& datas) {
	std::ifstream file(file_path);
	if (!file) {
		std::cerr << "cannot open file : " << file_path << std::endl;
		return 0;
	}
	int line=0;
	std::string str; 
	while (getline(file, str)) {
		Data d;
		line++;

		int parse_status = str_to_data(str,d);
		#ifdef VERBOSE
			if ( parse_status == STR_TO_DATA_INCOMPLETE) {
				std::cout << "warning : incomplete data in line " << line << std::endl;
			}
		#endif
		if (parse_status == STR_TO_DATA_FAILURE) {
			std::cout << "error : failed to parse line " << line << std::endl;
		}
		if ( parse_status == STR_TO_DATA_SUCCESS || parse_status == STR_TO_DATA_INCOMPLETE ) {
			datas.push_back(d);
		}
	}
	return 1;
}


int get_data_from_file_range(std::string file_path,std::vector<Data>& datas, unsigned int beginning, unsigned int count) {
	std::ifstream file(file_path);
	if (!file) {
		std::cerr << "cannot open file : " << file_path << std::endl;
		return 0;
	}
	if (beginning == 0) { beginning = 1; }
	int line=beginning-1;
	std::string str;
	while (beginning-- -1) {
		getline(file, str);
	}
	while (getline(file, str) && count--) {
		Data d;
		line++;

		int parse_status = str_to_data(str,d);
		#ifdef VERBOSE
			if (parse_status == STR_TO_DATA_INCOMPLETE) {
				std::cout << "warning : incomplete data in line " << line << std::endl;
			}
		#endif
		if (parse_status == STR_TO_DATA_FAILURE) {
			std::cout << "error : failed to parse line " << line << std::endl;
		}
		if ( parse_status == STR_TO_DATA_SUCCESS || parse_status == STR_TO_DATA_INCOMPLETE ) {
			datas.push_back(d);
		}
	}
	return 1;
}

int get_data_from_file_from(std::string file_path,std::vector<Data>& datas, unsigned int beginning) {
	std::ifstream file(file_path);
	if (!file) {
		std::cerr << "cannot open file : " << file_path << std::endl;
		return 0;
	}
	if (beginning == 0) { beginning = 1; }
	int line=beginning-1;
	std::string str;
	while (beginning-- -1) {
		getline(file, str);
	}
	while (getline(file, str)) {
		Data d;
		line++;

		int parse_status = str_to_data(str,d);
		#ifdef VERBOSE
			if (parse_status == STR_TO_DATA_INCOMPLETE) {
				std::cout << "warning : incomplete data in line " << line << std::endl;
			}
		#endif
		if (parse_status == STR_TO_DATA_FAILURE) {
			std::cout << "error : failed to parse line " << line << std::endl;
		}
		if ( parse_status == STR_TO_DATA_SUCCESS || parse_status == STR_TO_DATA_INCOMPLETE ) {
			datas.push_back(d);
		}
	}
	return 1;
}

int get_data_from_file_until(std::string file_path,std::vector<Data>& datas, unsigned  int count) {
	return get_data_from_file_range(file_path, datas, 0, count);
}