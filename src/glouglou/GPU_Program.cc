#include "GPU_Program.h"

#include <glm/gtc/type_ptr.hpp>
#include <iostream>

Glouglou::GPU_Program::GPU_Program() {
	program_id = glCreateProgram();
}

Glouglou::GPU_Program::~GPU_Program() {
	glDeleteProgram(program_id);
}

void Glouglou::GPU_Program::set_fragment_shader(std::string source) {
	fragment_shader = source;
}

void Glouglou::GPU_Program::set_geometry_shader(std::string source) {
	geometry_shader = source;
}

void Glouglou::GPU_Program::set_vertex_shader(std::string source) {
	vertex_shader = source;
}

void Glouglou::GPU_Program::compile() {
	GLuint vertex_shader_id   = glCreateShader(GL_VERTEX_SHADER);
	GLuint geometry_shader_id = glCreateShader(GL_GEOMETRY_SHADER);
	GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

	// vertex shader
	const GLchar *vertexShader_c_str = vertex_shader.c_str();
	glShaderSource(vertex_shader_id, 1, &vertexShader_c_str, NULL);
	glCompileShader(vertex_shader_id);
	check_shader_error(vertex_shader_id, "Shader error : Vertex shader compilation failed.");

	// geometry shader
	if(geometry_shader.size()) {
		const GLchar *geometryShader_c_str = geometry_shader.c_str();
		glShaderSource(geometry_shader_id, 1, &geometryShader_c_str, NULL);
		glCompileShader(geometry_shader_id);
		check_shader_error(geometry_shader_id, "Shader error : Geometry shader compilation failed.");
	}

	// fragment shader
	const GLchar *fragmentShader_c_str = fragment_shader.c_str();
	glShaderSource(fragment_shader_id, 1, &fragmentShader_c_str, NULL);
	glCompileShader(fragment_shader_id);
	check_shader_error(fragment_shader_id, "Shader error : Fragment shader compilation failed.");

	// linking program
	glAttachShader(program_id, vertex_shader_id);
	if(geometry_shader.size()) {
		glAttachShader(program_id, geometry_shader_id);
	}
	glAttachShader(program_id, fragment_shader_id);
	glLinkProgram(program_id);
	check_program_error(program_id, "Shader error : program link failed.");

	// once linked into a program, we no longer need the shaders.
	glDeleteShader(vertex_shader_id);
	glDeleteShader(geometry_shader_id);
	glDeleteShader(fragment_shader_id);

}

void Glouglou::GPU_Program::set_uniform(std::string name, float value) {
	glUniform1f(get_uniform_id(name),value);
}

// void Glouglou::GPU_Program::set_uniform(std::string name, double value) {
// 	glUniform1d(get_uniform_id(name),value);
// }

void Glouglou::GPU_Program::set_uniform(std::string name, glm::vec2 value) {
	glUniform2f(get_uniform_id(name),value.x,value.y);
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::vec3 value) {
	glUniform3f(get_uniform_id(name),value.x,value.y,value.z);
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::vec4 value) {
	glUniform4f(get_uniform_id(name),value.x,value.y,value.z,value.w);
}

void Glouglou::GPU_Program::set_uniform(std::string name, int value) {
	glUniform1i(get_uniform_id(name),value);
}

void Glouglou::GPU_Program::set_uniform(std::string name, bool value) {
	set_uniform(name,value?1:0);
}


void Glouglou::GPU_Program::set_uniform(std::string name, glm::ivec2 value) {
	glUniform2i(get_uniform_id(name),value.x,value.y);
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::ivec3 value) {
	glUniform3i(get_uniform_id(name),value.x,value.y,value.z);
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::ivec4 value) {
	glUniform4i(get_uniform_id(name),value.x,value.y,value.z,value.w);
}

void Glouglou::GPU_Program::set_uniform(std::string name, std::vector<float> value) {
	glUniform1fv(get_uniform_id(name),value.size(),value.data());
}

// void Glouglou::GPU_Program::set_uniform(std::string name, std::vector<double> value) {
// 	glUniform1dv(get_uniform_id(name),value.size(),value.data());
// }

void Glouglou::GPU_Program::set_uniform(std::string name, std::vector<int> value) {
	glUniform1iv(get_uniform_id(name),value.size(),value.data());
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::mat2 value) {
	glUniformMatrix2fv(get_uniform_id(name),1,GL_FALSE,glm::value_ptr(value));
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::mat3 value) {
	glUniformMatrix3fv(get_uniform_id(name),1,GL_FALSE,glm::value_ptr(value));
}

void Glouglou::GPU_Program::set_uniform(std::string name, glm::mat4 value) {
	glUniformMatrix4fv(get_uniform_id(name),1,GL_FALSE,glm::value_ptr(value));
}

void Glouglou::GPU_Program::set_uniform(std::string name, Glouglou::GPU_Texture& tex) {
	tex.set_uniform(get_uniform_id(name));
}

GLuint Glouglou::GPU_Program::gl_id(){
	return program_id;
}

void Glouglou::GPU_Program::use() {
	glUseProgram(program_id);
}

void Glouglou::GPU_Program::check_shader_error(GLuint shaderId, const std::string& exceptionMsg) {
	GLint result = GL_FALSE;
	int infoLength = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLength);
	if (result == GL_FALSE) {
		std::vector<GLchar> errorMessage(infoLength + 1);
		glGetShaderInfoLog(shaderId, infoLength, NULL, &errorMessage[0]);
		std::cerr << &errorMessage[0] << std::endl << std::flush;
		throw std::runtime_error(exceptionMsg);
	}
}

void Glouglou::GPU_Program::check_program_error(GLuint program_id, const std::string& exceptionMsg) {
	GLint result = GL_FALSE;
	int infoLength = 0;
	glGetProgramiv(program_id, GL_LINK_STATUS, &result);
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infoLength);
	if (result == GL_FALSE) {
		std::vector<GLchar> errorMessage(infoLength + 1);
		glGetProgramInfoLog(program_id, infoLength, NULL, &errorMessage[0]);
		std::cerr << &errorMessage[0] << std::endl << std::flush;
		throw std::runtime_error(exceptionMsg);
	}
}

GLint Glouglou::GPU_Program::get_uniform_id(std::string name) {
	const char* name_c_str = name.c_str();
	int id = glGetUniformLocation(program_id, name_c_str);
	if(id<0) {
		std::cout << "GPU_Program warning : this uniform does not exist :" + name << std::endl << std::flush;
		// throw std::runtime_error("this uniform does not exist :" + name);
	}
	return id;
}


