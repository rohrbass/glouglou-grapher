#include "gl_includes.h"
#include "GPU_Data.h"
#include <vector>
#include <iostream>
#include <string>

Glouglou::GPU_Data::GPU_Data() {
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &elementbuffer);
}

void Glouglou::GPU_Data::to_VRAM(const std::vector<float>& vertices, const std::vector<unsigned int>& indices, unsigned int data_dimension) {
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		this->data_dimension = data_dimension;
		vertices_count = vertices.size();
		indices_count = indices.size();
		glBufferData(GL_ARRAY_BUFFER, vertices_count*sizeof(float), vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_count*sizeof(float), indices.data(), GL_STATIC_DRAW);

		// cf [man glBufferData]
		// these do not affect usage, but performace
		// GL_STREAM_DRAW  GL_STATIC_DRAW  GL_DYNAMIC_DRAW
		// GL_STREAM_READ  GL_STATIC_READ  GL_DYNAMIC_READ
		// GL_STREAM_COPY  GL_STATIC_COPY  GL_DYNAMIC_COPY


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//		OK	│GL_ARRAY_BUFFER              │ Vertex attributes
	//		OK	│GL_ELEMENT_ARRAY_BUFFER      │ Vertex array indices
	//		??	│GL_ATOMIC_COUNTER_BUFFER     │ Atomic counter storage
	//		??	│GL_COPY_READ_BUFFER          │ Buffer copy source
	//		??	│GL_COPY_WRITE_BUFFER         │ Buffer copy destination
	//		??	│GL_DISPATCH_INDIRECT_BUFFER  │ Indirect compute dispatch commands
	//		??	│GL_DRAW_INDIRECT_BUFFER      │ Indirect command arguments
	//		??	│GL_PIXEL_PACK_BUFFER         │ Pixel read target
	//		??	│GL_PIXEL_UNPACK_BUFFER       │ Texture data source
	//		??	│GL_TEXTURE_BUFFER            │ Texture data buffer
	//		??	│GL_QUERY_BUFFER              │ Query result buffer
	//		??	│GL_SHADER_STORAGE_BUFFER     │ Read-write storage for shaders
	//		??	│GL_TRANSFORM_FEEDBACK_BUFFER │ Transform feedback buffer
	//		??	│GL_UNIFORM_BUFFER            │ Uniform block storage
}

void Glouglou::GPU_Data::to_VRAM(const std::vector<float>& vertices, unsigned int data_dimension) {
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		this->data_dimension = data_dimension;
		vertices_count = vertices.size();
		indices_count = 0; // indices.size();
		glBufferData(GL_ARRAY_BUFFER, vertices_count*sizeof(float), vertices.data(), GL_STATIC_DRAW);
		// glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_count*sizeof(float), indices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

#include "glm_helper.h"

// void Glouglou::GPU_Data::to_VRAM(const std::vector<glm::vec2>& vertices) {
// 	std::vector<float> v = vect_vecn_to_vect_float(vertices);
// 	to_VRAM(v,2);
// }
// void Glouglou::GPU_Data::to_VRAM(const std::vector<glm::vec3>& vertices) {
// 	std::vector<float> v = vect_vecn_to_vect_float(vertices);
// 	to_VRAM(v,3);
// }
// void Glouglou::GPU_Data::to_VRAM(const std::vector<glm::vec2>& vertices, const std::vector<unsigned int>& indices) {
// 	std::vector<float> v = vect_vecn_to_vect_float(vertices);
// 	to_VRAM(v, indices, 2);
// }
void Glouglou::GPU_Data::to_VRAM(const std::vector<glm::vec3>& vertices, const std::vector<unsigned int>& indices) {
	std::vector<float> v = vect_vecn_to_vect_float(vertices);
	to_VRAM(v, indices, 3);
}

Glouglou::GPU_Data::~GPU_Data() {
	glDeleteBuffers(1,&vbo);
	glDeleteBuffers(1,&elementbuffer);
}
