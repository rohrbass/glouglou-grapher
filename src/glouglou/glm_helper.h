#pragma once

#include <glm/glm.hpp>

std::vector<float> vect_vecn_to_vect_float(const std::vector<glm::vec2>& v_in) {
	// how should I return this to avoid a copy, but not get ugly ?
	std::vector<float> v;
	for (size_t i=0;i<v_in.size();i++){
		v.push_back(v_in[i][0]);
		v.push_back(v_in[i][1]);
	}
	return v;
}

std::vector<float> vect_vecn_to_vect_float(const std::vector<glm::vec3>& v_in) {
	// how should I return this to avoid a copy, but not get ugly ?
	std::vector<float> v;
	for (size_t i=0;i<v_in.size();i++){
		v.push_back(v_in[i][0]);
		v.push_back(v_in[i][1]);
		v.push_back(v_in[i][2]);
	}
	return v;
}



std::vector<glm::vec2> vect_float_to_vect_vec2(const std::vector<glm::vec2>&){
	// how should I return this to avoid a copy, but not get ugly ?
	return std::vector<glm::vec2>();
}

std::vector<glm::vec3> vect_float_to_vect_vec3(const std::vector<glm::vec2>&){
	// how should I return this to avoid a copy, but not get ugly ?
	return std::vector<glm::vec3>();
}
