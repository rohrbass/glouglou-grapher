#include "Context.h"

// Glouglou::Context::Context(int width, int height, int gl_major_version, int gl_minor_version) {
Glouglou::Context::Context(glm::vec2 window_dim, int gl_major_version, int gl_minor_version) {
	SDL_Init(SDL_INIT_VIDEO);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_version);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_version);
	// sdl_window = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	// sdl_window = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
	sdl_window = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_dim.x, window_dim.y, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
	gl_context = SDL_GL_CreateContext(sdl_window);

	glewExperimental = GL_TRUE;
	glewInit();

	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClearDepth( 1.0f );

	glEnable(GL_PROGRAM_POINT_SIZE);
	// glPointSize

	// glEnable(GL_LINE_SMOOTH);
	// glEnable(GL_POINT_SMOOTH);
	// glEnable(GL_POLYGON_SMOOTH); // !! an alpha buffer is needed and the polygons must be sorted front to back.
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS); // uses more memory, to disable if not needed

	glEnable(GL_DEPTH_TEST);
	// glDepthFunc GL_NEVER | GL_LESS | GL_EQUAL | GL_LEQUAL | GL_GREATER | GL_NOTEQUAL | GL_GEQUAL | GL_ALWAYS

	// TODO : WTF is dat ???
	glEnable(GL_DEPTH_CLAMP); // If enabled, the −wc≤zc≤wc-wc≤zc≤wc plane equation is ignored by view volume clipping (effectively, there is no near or far plane clipping).
	// glDepthRange

	// glEnable(GL_POLYGON_OFFSET_FILL); // If enabled, and if the polygon is rendered in GL_FILL mode, an offset is added to depth values of a polygon's fragments before the depth comparison is performed. See glPolygonOffset.
	// glEnable(GL_POLYGON_OFFSET_LINE); // If enabled, and if the polygon is rendered in GL_LINE mode, an offset is added to depth values of a polygon's fragments before the depth comparison is performed. See glPolygonOffset.
	// glEnable(GL_POLYGON_OFFSET_POINT);// If enabled, an offset is added to depth values of a polygon's fragments before the depth comparison is performed, if the polygon is rendered in GL_POINT mode. See glPolygonOffset.
	// glPolygonOffset

	// glEnable(GL_BLEND);
	// glBlendFunc
	// glBlendColor
	// glBlendEquation
	// glBlendEquationSeparate
	// glBlendFuncSeparate


	glEnable(GL_CULL_FACE);
	// glCullFace GL_FRONT | GL_BACK | GL_FRONT_AND_BACK

	//TODO : check quality improvement VS performance
	glEnable(GL_DITHER); // If enabled, dither color components or indices before they are written to the color buffer.


// glEnable(GL_CLIP_DISTANCEi);
// If enabled, clip geometry against user-defined half space i.

// glEnable(GL_COLOR_LOGIC_OP);
// If enabled, apply the currently selected logical operation to the computed fragment color and color buffer values.
// glLogicOp GL_CLEAR | GL_SET | GL_COPY | GL_COPY_INVERTED | GL_NOOP | GL_INVERT | GL_AND | GL_NAND | GL_OR | GL_NOR | GL_XOR | GL_EQUIV | GL_AND_REVERSE | GL_AND_INVERTED | GL_OR_REVERSE | GL_OR_INVERTED.
//           The initial value is GL_COPY.

// glEnable(GL_FRAMEBUFFER_SRGB);
// If enabled and the value of GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING for the framebuffer attachment corresponding to the destination buffer is GL_SRGB, the R, G, and B destination color values (after conversion from fixed-point to floating-point) are considered to be encoded for the sRGB color space and hence are linearized prior to their use in blending.


// glEnable(GL_MULTISAMPLE); // If enabled, use multiple fragment samples in computing the final color of a pixel.
// glEnable(GL_SAMPLE_COVERAGE); // If enabled, the fragment's coverage is ANDed with the temporary coverage value. If GL_SAMPLE_COVERAGE_INVERT is set to GL_TRUE, invert the coverage value.
// glSampleCoverage

// glEnable(GL_PRIMITIVE_RESTART);
// Enables primitive restarting. If enabled, any one of the draw commands which transfers a set of generic attribute array elements to the GL will restart the primitive when the index of the vertex is equal to the primitive restart index.
// glPrimitiveRestartIndex

// glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
// If enabled, compute a temporary coverage value where each bit is determined by the alpha value at the corresponding sample location. The temporary coverage value is then ANDed with the fragment coverage value.

// glEnable(GL_SAMPLE_ALPHA_TO_ONE);
// If enabled, each sample alpha value is replaced by the maximum representable alpha value.


// glEnable(GL_SCISSOR_TEST);
// If enabled, discard fragments that are outside the scissor rectangle.
// glScissor

// glEnable(GL_STENCIL_TEST);
// If enabled, do stencil testing and update the stencil buffer.
// glStencilFunc
// glStencilOp

}

void Glouglou::Context::set_size(glm::vec2 size) {
	SDL_SetWindowSize(sdl_window, size.x, size.y);
}

glm::vec2 Glouglou::Context::get_size() {
	int w;
	int h;
	SDL_GetWindowSize(sdl_window, &w, &h);
	return glm::vec2(w,h);
}

Glouglou::Context::~Context() {
	SDL_DestroyWindow(sdl_window);
	sdl_window = NULL;
	SDL_Quit();	
}

void Glouglou::Context::swap() {
	SDL_GL_SwapWindow(sdl_window);
}
