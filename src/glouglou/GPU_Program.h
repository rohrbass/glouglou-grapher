#pragma once

#include "gl_includes.h"
#include "GPU_Texture.h"

#include <glm/glm.hpp>
#include <vector>
#include <string>

namespace Glouglou {
	// TODO : make it possible to reuse same shaders
	// - Shader for generic shader fragments (compile any glsl)
	// - Shader_frag
	// - Shader_geom
	// - Shader_tess_ctrl
	// - Shader_tess_eval
	// - Shader_vert
	// - Shader_program that links the created shader
	// - Shader_program_from_source that behaves like the Shader class as it is now

	// TODO
	// would be awesome : typecheck set_uniform after shader compilation


	class GPU_Program {
	public:
		GPU_Program();
		~GPU_Program();
		void set_fragment_shader(std::string source);
		void set_geometry_shader(std::string source);
		// TODO
		// void set_tesselation_shader(std::string source);
		// TODO
		// void set_compute_shader(std::string source);
		void set_vertex_shader(std::string source);
		void compile();
		///////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////// UNIFORM OVERLOADING ///////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////

		void set_uniform(std::string name, float value);
		// disabled yet : only for opengl4
		// void set_uniform(std::string name, double value);
		void set_uniform(std::string name, glm::vec2 value);
		void set_uniform(std::string name, glm::vec3 value);
		void set_uniform(std::string name, glm::vec4 value);

		void set_uniform(std::string name, int value);
		void set_uniform(std::string name, bool value);
		void set_uniform(std::string name, glm::ivec2 value);
		void set_uniform(std::string name, glm::ivec3 value);
		void set_uniform(std::string name, glm::ivec4 value);

		void set_uniform(std::string name, std::vector<float> value);
		// disabled yet : only for opengl4
		// void set_uniform(std::string name, std::vector<double> value);

		// TODO 
		// void set_uniform(std::string name, std::vector<glm::vec2> value);
		// void set_uniform(std::string name, std::vector<glm::vec3> value);
		// void set_uniform(std::string name, std::vector<glm::vec4> value);

		void set_uniform(std::string name, std::vector<int> value);
		// TODO 
		// void set_uniform(std::string name, std::vector<glm::ivec2> value);
		// void set_uniform(std::string name, std::vector<glm::ivec3> value);
		// void set_uniform(std::string name, std::vector<glm::ivec4> value);

		void set_uniform(std::string name, glm::mat2 value);
		void set_uniform(std::string name, glm::mat3 value);
		void set_uniform(std::string name, glm::mat4 value);

		// TEXTURE --- -- -- - -  -   -    -
		// TODO
		void set_uniform(std::string name, Glouglou::GPU_Texture& tex);
		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		void use();

		GLuint gl_id();
	private:
		GPU_Program(const GPU_Program&) = delete;
		GPU_Program& operator=(const GPU_Program&) = delete;
		GLuint program_id = 0;
		std::string fragment_shader;
		std::string geometry_shader;
		std::string vertex_shader;
		void check_shader_error(GLuint GPU_ProgramId, const std::string& exceptionMsg);
		void check_program_error(GLuint program_id, const std::string& exceptionMsg);
		GLint get_uniform_id(std::string name);
	};
}
