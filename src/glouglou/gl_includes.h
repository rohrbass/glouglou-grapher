#pragma once

#ifdef __APPLE__
	#include <OpenGL/glew.h>
	#include <OpenGL/gl.h>
#else
	#include <GL/glew.h>
	#include <GL/gl.h>
#endif
