#include "GPU_Program_Runner.h"
#include "GPU_Data.h"
#include "GPU_Program.h"
// #include "gl_includes.h"

#include <vector>
#include <iostream>
#include <string>


Glouglou::GPU_Program_Runner::GPU_Program_Runner(Glouglou::GPU_Program &program) {
	prog = &program;
	datas = NULL;
	glGenVertexArrays(1, &vao);
}

Glouglou::GPU_Program_Runner::~GPU_Program_Runner() {
	glDeleteVertexArrays(1,&vao);		
}

void Glouglou::GPU_Program_Runner::link_data(Glouglou::GPU_Data &data, std::string name) {
	GLuint shader_attribute_index = glGetAttribLocation(prog->gl_id(), name.c_str());
	if(shader_attribute_index == (GLuint)-1) {
		std::cout << "link_data failed : " << name << " does not exist in the shader." << std::endl;
		return;
	}
	link_data(data,shader_attribute_index);
}

void Glouglou::GPU_Program_Runner::link_data(Glouglou::GPU_Data &data, GLuint shader_attribute_index) {
	datas = &data;
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, data.vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.elementbuffer);

		glEnableVertexAttribArray(shader_attribute_index);
		glVertexAttribPointer(
			shader_attribute_index, // pos_attribute to set
			data.data_dimension,   // dimension
			GL_FLOAT,               // type
			GL_FALSE,               // normalize ?
			0,                      // stride
			0                       // offset
		);
	glBindVertexArray(0);
}

#define draw() \
	glBindVertexArray(vao);\
	glDrawElements(\
		GL_TRIANGLES, datas->indices_count, GL_UNSIGNED_INT, (void*)0 /* element array buffer offset*/\
	);\
	glBindVertexArray(0);\

void Glouglou::GPU_Program_Runner::draw_triangles() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	draw();
}

void Glouglou::GPU_Program_Runner::draw_triangles_edges() {
	// TODO : optimize mesh to not render shared edges twice
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	draw();
}

void Glouglou::GPU_Program_Runner::draw_edges() {
	glBindVertexArray(vao);
	glDrawElements(
		GL_LINES, datas->indices_count, GL_UNSIGNED_INT, (void*)0 /* element array buffer offset*/
		// GL_LINE_STRIP, datas->indices_count, GL_UNSIGNED_INT, (void*)0 /* element array buffer offset*/
		// GL_TRIANGLE_FAN, datas->indices_count, GL_UNSIGNED_INT, (void*)0 /* element array buffer offset*/
	);
	glBindVertexArray(0);
}

void Glouglou::GPU_Program_Runner::draw_points() {
	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, datas->vertices_count/datas->data_dimension);
	glBindVertexArray(0);
}

void Glouglou::GPU_Program_Runner::draw_points(size_t n) {
	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, n);
	glBindVertexArray(0);
}
#undef draw
