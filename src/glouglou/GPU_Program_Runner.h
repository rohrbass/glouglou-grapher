#pragma once

#include "glouglou.h"
#include <string>
// #include "GPU_Data.h"
// #include "GPU_Program.h"

namespace Glouglou {

	class GPU_Data;
	class GPU_Program;

	class GPU_Program_Runner {
	public:
		GPU_Program_Runner(Glouglou::GPU_Program &program);
		~GPU_Program_Runner();

		void link_data(Glouglou::GPU_Data &data, std::string name);
		void link_data(Glouglou::GPU_Data &data, GLuint shader_attribute_index);

		void draw_triangles();
		void draw_triangles_edges();
		void draw_edges();
		void draw_points();
		void draw_points(size_t n);
	private:
		GLuint vao;
		Glouglou::GPU_Data *datas;
		Glouglou::GPU_Program *prog;
	};

}
