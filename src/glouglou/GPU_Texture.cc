#include "GPU_Texture.h"

#include <list>

static bool TextureInitialized = false;
static std::list<int> available_tex_index;

Glouglou::GPU_Texture::GPU_Texture() {
	if(!TextureInitialized) {
		for(int i=0;i<GL_MAX_TEXTURE_UNITS;i++) {
			available_tex_index.push_back(i);
		}
		TextureInitialized = true;
	}
	tex_index = available_tex_index.front();
	available_tex_index.pop_front();
	glGenTextures(1,&id);
	set_interpolation_mode_linear();
}

void Glouglou::GPU_Texture::set_interpolation_mode_linear() {
	glBindTexture(GL_TEXTURE_2D, id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Glouglou::GPU_Texture::set_interpolation_mode_nearest() {
	glBindTexture(GL_TEXTURE_2D, id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Glouglou::GPU_Texture::update_content_RGBA(GLubyte* data, GLuint width, GLuint height) {
	glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,  width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Glouglou::GPU_Texture::update_content_depth(GLfloat* data, GLuint width, GLuint height) {
	glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height,0, GL_DEPTH_COMPONENT, GL_FLOAT,data);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Glouglou::GPU_Texture::set_uniform(GLint uniform_id) {
	glActiveTexture(GL_TEXTURE0 + tex_index);
	glUniform1i(uniform_id, /*GL_TEXTURE*/tex_index);
	glBindTexture(GL_TEXTURE_2D, id);
}

Glouglou::GPU_Texture::~GPU_Texture() {
	glDeleteTextures(1, &id);
	available_tex_index.push_front(tex_index);
}

