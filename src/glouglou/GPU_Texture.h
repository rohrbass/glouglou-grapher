#pragma once

#include "gl_includes.h"

namespace Glouglou {
	class GPU_Texture {
	public:
		GPU_Texture();
		~GPU_Texture();
		void set_interpolation_mode_linear();
		void set_interpolation_mode_nearest();
		void update_content_RGBA(GLubyte* data, GLuint width, GLuint height);
		void update_content_depth(GLfloat* data, GLuint width, GLuint height);
		// this should disappear as public, use friend instead
		void set_uniform(GLint uniform_id);
	private:
		int tex_index;
		GLuint id = 0;
	};
}
