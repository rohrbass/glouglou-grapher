#pragma once

#include "GPU_Program_Runner.h"
#include <glm/glm.hpp>
#include <vector>

// note : for vertices and normal, create two GPU_Data
//        this does make more sense since you could
//        want to include any other input such as velocity,
//        position_before, temperature, color, dust_amount,
//        pretty_much_anything
// note : for now this is inefficient as indices
//        might be uploaded multiple times with
//        exactly the same content.
//           - solution 1 : share indices
//           - solution 2 : make a mesh constructor.
//                          but this is out of the scope
//                          of the project for now (mesh editor)
namespace Glouglou {

	class GPU_Program_Runner;

	class GPU_Data {
	friend class Glouglou::GPU_Program_Runner;
	public:
		GPU_Data();
		void to_VRAM(const std::vector<float>& vertices, unsigned int data_dimension=1);
		// void to_VRAM(const std::vector<glm::vec2>& vertices);
		// void to_VRAM(const std::vector<glm::vec3>& vertices);
		void to_VRAM(const std::vector<float>& vertices, const std::vector<unsigned int>& indices, unsigned int data_dimension=1);
		// void to_VRAM(const std::vector<glm::vec2>& vertices, const std::vector<unsigned int>& indices);
		void to_VRAM(const std::vector<glm::vec3>& vertices, const std::vector<unsigned int>& indices);
		~GPU_Data();
	private:
		unsigned int data_dimension;
		GLuint vbo;
	 	GLuint elementbuffer;
		GLuint vertices_count;
		GLuint indices_count;
	};
}
