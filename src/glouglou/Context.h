#pragma once

#include "gl_includes.h"
#include <SDL2/SDL.h>
// #include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>

namespace Glouglou {
	// builds a window with an opengl context
	// default to opengl3.3 at 800x600
	//
	// !!! important note : 
	//     1. do create a window first to have an opengl context so that shader texture etc... actually do something
	//     2. do not forget to swap window after the drawing calls
	class Context {
	public:
		// Context(int width=800, int height=600, int gl_major_version=3, int gl_minor_version=3);
		Context(glm::vec2 window_dim=glm::vec2(800,600), int gl_major_version=3, int gl_minor_version=3);
		~Context();
		void swap();
		void set_size(glm::vec2 size);
		glm::vec2 get_size();
		// TODO
		// set_icon(image);
		// std::string get_title();
		// void set_title(std::string);
	private:
		// class impl;
		// unique_ptr<impl> pimpl;
		// widget::widget() : pimpl{ new impl{ /*...*/ } } { }
		SDL_Window* sdl_window = NULL;
		SDL_GLContext gl_context;	
	};
}
