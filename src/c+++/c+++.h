#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

// this does return an empty string on inexistant file
std::string file_to_str(const std::string path_to_file);
