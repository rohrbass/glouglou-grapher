#include "c+++.h"

std::string file_to_str(const std::string path_to_file) {
	std::ifstream in(path_to_file);
	if(!in.is_open()) {
		std::cerr << "ERROR : could not open " << path_to_file << std::endl;
		// throw "test";
	}
	std::stringstream buffer;
	buffer << in.rdbuf();
	return buffer.str();
}
