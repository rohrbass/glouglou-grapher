#include "compilation_option.h"

#include "glouglou/glouglou.h"
#include "glouglou_helper/glouglou_geometry.h"

#include "laurent_eyer_data/data_parser.h"
#include "laurent_eyer_data/data_treatment.h"
#include <algorithm>
// #include <map>

#include "c+++/c+++.h"
// #include <vector>
// #include <string>
// #include <unistd.h>

using namespace std;
using namespace glm;

float dither_noise(float range) {
	return range - 2*range * (static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
}
bool a_order_data_by_night (const Data& d1,const Data& d2) {
	return d1.NUIT < d2.NUIT;
}

int main(int argc, char* argv[]) {
	string file_path;
	if (argc==1) {
		cout<<"usage : "<< argv[0] <<" [path_to_file]"<<endl;
		return -1;
	} else {
		file_path = argv[1];
	}

	// -------------------
	// step 1 : load data

	vector<Data> datas;
	get_data_from_file_from(file_path,datas,3);
	// get_data_from_file_range(file_path,datas,3,100000);

	// -------------------
	// step 2 : sort data

	// it's already sorded by id
	// sort(datas.begin(),datas.end(),sort_call_back_data_by_id);

	// this is useless for the representation I'm trying now
	sort(datas.begin(),datas.end(),a_order_data_by_night);


	// vector<int>		 laurent_data_vector_CODE;
	// vector<int>		 laurent_data_vector_NUIT;
	// vector<float>	 laurent_data_vector_PJ;
	// vector<float>	 laurent_data_vector_FZ;
	// vector<int>		 laurent_data_vector_IPV;
	// vector<float>	 laurent_data_vector_VM;
	// vector<int>		 laurent_data_vector_IPC;
	vector<float>	 laurent_data_vector_U;
	vector<float>	 laurent_data_vector_V;
	// vector<float>	 laurent_data_vector_B1;
	// vector<float>	 laurent_data_vector_B2;
	// vector<float>	 laurent_data_vector_V1;
	// vector<float>	 laurent_data_vector_G;

	for (size_t i = 0; i < datas.size(); i++) {
		// enlever si IPV ou IPC == 0 1 ou 2
		if (datas[i].IPV == 0 || datas[i].IPV == 1 || datas[i].IPV == 2 ||
			datas[i].IPC == 0 || datas[i].IPC == 1 || datas[i].IPC == 2) {
			// laurent_data_vector_CODE.push_back( datas[i].CODE );
			// laurent_data_vector_NUIT.push_back( datas[i].NUIT );
			// laurent_data_vector_PJ.push_back(   datas[i].PJ   );
			// laurent_data_vector_FZ.push_back(   datas[i].FZ   );
			// laurent_data_vector_IPV.push_back(  datas[i].IPV  );
			// laurent_data_vector_VM.push_back(   datas[i].VM   );
			// laurent_data_vector_IPC.push_back(  datas[i].IPC  );
			laurent_data_vector_U.push_back(    -datas[i].V    + dither_noise(0.0005) );
			laurent_data_vector_V.push_back(    -datas[i].U    + dither_noise(0.0005) );
			// laurent_data_vector_B1.push_back(   datas[i].B1   );
			// laurent_data_vector_B2.push_back(   datas[i].B2   );
			// laurent_data_vector_V1.push_back(   datas[i].V1   );
			// laurent_data_vector_G.push_back(    datas[i].G    );
		}
	}

	// for(int i=0;i<laurent_data_vector_NUIT.size()-1;i++) {
	// 	if(laurent_data_vector_NUIT[i]>laurent_data_vector_NUIT[i+1]) {
	// 		cout<<"ERROR"<<endl;
	// 		return 1;
	// 	}
	// }




//========================================================
//= STEP 0 : CREATE A CONTEXT ============================
//========================================================

	Glouglou::Context ctx = Glouglou::Context(vec2(400,300),3,3);

//========================================================
//= SETP 1 : MAKE A PROGRAM ON THE GPU ===================
//========================================================

	glDisable(GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// glBlendFunc(GL_ZERO, GL_SRC_COLOR);
	// glBlendFunc(GL_ONE, GL_ONE);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	Glouglou::GPU_Program shader_dots;
		// shader.set_vertex_shader( file_to_str("shader/identity.vert"));
		// shader.set_fragment_shader( file_to_str("shader/noise.frag"));
		shader_dots.set_vertex_shader( file_to_str("shader/laurent_data_zoom.vert"));
		shader_dots.set_fragment_shader( file_to_str("shader/color.frag"));
		// shader_dots.set_geometry_shader( file_to_str("shader/laurent_eyer_points.geom"));
		shader_dots.compile();

	Glouglou::GPU_Program shader_square;
		// shader.set_vertex_shader( file_to_str("shader/identity.vert"));
		// shader.set_fragment_shader( file_to_str("shader/noise.frag"));
		shader_square.set_vertex_shader( file_to_str("shader/laurent_data_zoom.vert"));
		shader_square.set_fragment_shader( file_to_str("shader/color.frag"));
		shader_square.set_geometry_shader( file_to_str("shader/laurent_eyer_points.geom"));
		shader_square.compile();


//========================================================
//= STEP 2 : SEND DATA TO THE GPU ========================
//========================================================

	// Glouglou::GPU_Data laurent_data_CODE;	//int ;
	// Glouglou::GPU_Data laurent_data_NUIT;	//int ;
	// Glouglou::GPU_Data laurent_data_PJ;		//float ;
	// Glouglou::GPU_Data laurent_data_FZ;		//float ;
	// Glouglou::GPU_Data laurent_data_IPV;		//int ;
	// Glouglou::GPU_Data laurent_data_VM;		//float ;
	// Glouglou::GPU_Data laurent_data_IPC;		//int ;
	Glouglou::GPU_Data laurent_data_X;		//float ;
	Glouglou::GPU_Data laurent_data_Y;		//float ;
	// Glouglou::GPU_Data laurent_data_B1;		//float ;
	// Glouglou::GPU_Data laurent_data_B2;		//float ;
	// Glouglou::GPU_Data laurent_data_V1;		//float ;
	// Glouglou::GPU_Data laurent_data_G;		//float ;

	laurent_data_X.to_VRAM(laurent_data_vector_U);
	laurent_data_Y.to_VRAM(laurent_data_vector_V);

//========================================================
//= STEP 3 : SETUP PROGRAM CALL (LINK DATA TO GPU INPUT) =
//========================================================
	Glouglou::GPU_Program_Runner program_runner_square(shader_square);
	Glouglou::GPU_Program_Runner program_runner_dots(shader_dots);
		// program_runner.link_data(square_mesh,"position");
		// program_runner.link_data(square_temperature,"temperature");

		program_runner_square.link_data(laurent_data_X,"X");
		program_runner_square.link_data(laurent_data_Y,"Y");
		program_runner_dots.link_data(laurent_data_X,"X");
		program_runner_dots.link_data(laurent_data_Y,"Y");

//========================================================
//========================================================
//========================================================











	vec2 zoom;
	zoom.x=1;
	zoom.y=1;
	vec2 camera_position;
	camera_position.x=0;
	camera_position.y=0;

	size_t n=100;
	size_t n_max = laurent_data_vector_U.size();

	bool quit=false;
	SDL_Event e;
	const Uint8 *keyboard_state;
	float t=0;
	bool render_dots = true;
	float exposure =.2;
	while(!quit) {
		keyboard_state = SDL_GetKeyboardState(NULL);
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			if (e.type == SDL_MOUSEWHEEL) {
				if(keyboard_state[SDL_SCANCODE_LCTRL] ) {
					zoom.x *= pow(1.1,-e.wheel.x);
					zoom.y *= pow(1.1,e.wheel.y);
				} else if (keyboard_state[SDL_SCANCODE_LSHIFT]) {
					exposure += 0.01 * e.wheel.y;
					exposure = clamp(exposure , 0.01f , 1.f);
				}else {
					camera_position.x +=-0.02*e.wheel.x/zoom.x;
					camera_position.y += 0.02*e.wheel.y/zoom.y;
				}
			}
			if (e.type == SDL_WINDOWEVENT) {
				if(e.window.event == SDL_WINDOWEVENT_RESIZED) {
					glViewport(0,0,e.window.data1,e.window.data2);
				}
			}
			if (e.type == SDL_KEYDOWN) {
				if (e.key.keysym.sym == SDLK_p) {
					render_dots = !render_dots;
				}
			}
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// DRAW
		t+=0.001;
		n +=100;
		if ( n > n_max ) {
			n=1;
		}
		// if(zoom.x<1) {
		if( !render_dots ) {
			shader_square.use();
			// shader.set_uniform("time",t);
			shader_square.set_uniform("color",vec4(1.,1.,1.,exposure));
			shader_square.set_uniform("zoom_factor",zoom);
			shader_square.set_uniform("camera_position",camera_position);
			shader_square.set_uniform("geom_square_size",0.0005f);
			shader_square.set_uniform("resolution",ctx.get_size());


			// program_runner_square.draw_triangles();
			// program_runner_square.draw_edges();
			program_runner_square.draw_points();
		} else {
			shader_dots.use();
			// shader.set_uniform("time",t);
			shader_dots.set_uniform("color",vec4(1.,1.,1.,exposure));
			shader_dots.set_uniform("zoom_factor",zoom);
			shader_dots.set_uniform("camera_position",camera_position);
			// shader_dots.set_uniform("geom_square_size",0.0005f);


			// program_runner_dots.draw_triangles();
			// program_runner_dots.draw_edges();
			// program_runner_dots.draw_points();
			program_runner_dots.draw_points(n);
		}

		ctx.swap();
	}
	return 0;
}
