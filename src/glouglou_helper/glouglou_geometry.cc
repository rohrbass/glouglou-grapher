
#include "glouglou_geometry.h"

Square::Square(unsigned int dim) {
	if(dim==2) {
		vertices = {
		-1,-1,
		 1,-1,
		 1, 1,
		-1, 1
		};
	} else {
		vertices = {
		-1,-1,0,
		 1,-1,0,
		 1, 1,0,
		-1, 1,0
		};			
	}
	indices = {
		0,1,2,
		2,3,0
	};
	dimension = dim;
}
