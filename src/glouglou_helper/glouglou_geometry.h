#pragma once

#include <vector>

class Square {
public:
	Square(unsigned int dim=2);
// private:
	unsigned int dimension;
	std::vector<float> vertices;
	std::vector<unsigned int> indices;
};
