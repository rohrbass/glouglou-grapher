#include "compilation_option.h"

#include "glouglou/glouglou.h"
#include "glouglou_helper/glouglou_geometry.h"

#include "laurent_eyer_data/data_parser.h"
#include "laurent_eyer_data/data_treatment.h"
#include <algorithm>
// #include <map>

#include "c+++/c+++.h"
// #include <vector>
// #include <string>
// #include <unistd.h>

using namespace std;
using namespace glm;

float dither_noise(float range) {
	return range - 2*range * (static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
}

float random(float range) {
	return range * (static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
}

bool sort_by_PJ(const Data& d1,const Data& d2) {
	return d1.PJ < d2.PJ;
}

int main(int argc, char* argv[]) {
	string file_path;
	if (argc==1) {
		cout<<"usage : "<< argv[0] <<" [path_to_file]"<<endl;
		return -1;
	} else {
		file_path = argv[1];
	}

	// -------------------
	// step 1 : load data

	vector<Data> datas;
	get_data_from_file_from(file_path,datas,3);
	// get_data_from_file_range(file_path,datas,3,100000);

	// -------------------
	// step 2 : sort data

	// it's already sorded by id
	// sort(datas.begin(),datas.end(),sort_call_back_data_by_id);

	// this is useless for the representation I'm trying now
	// sort(datas.begin(),datas.end(),sort_call_back_data_by_night);

	// -------------------
	// step 3 : split data by ID (make a vector for each star)
	vector<vector<Data>> datas_split_by_id;
	vector<Data> tmp = vector<Data>(0);
	int datas_code = datas[0].CODE;
	cout<<"hello"<<flush;
	for (size_t i = 0; i < datas.size(); i++) {
		cout<<"."<<flush;
		if(datas[i].CODE == datas_code){
			tmp.push_back(datas[i]);
		} else {
			datas_code = datas[i].CODE;
			datas_split_by_id.push_back(tmp);
			tmp = vector<Data>({datas[i]});
		}
	}

	// sort data by data aquisition
	sort(datas_split_by_id.begin(),datas_split_by_id.end(),sort_call_back_vector_by_size<Data>);

	// for(size_t i=0;i<datas_split_by_id.size();i++){
	// 	cout<< datas_split_by_id[i].size()<<endl<<flush;
	// }
	// cout << datas_split_by_id[0].size() << endl << flush;
	// cout << datas_split_by_id[1].size() << endl << flush;

	// cout<< datas_split_by_id[0].size()<<endl<<flush;
	// cout<< datas_split_by_id[datas_split_by_id.size()-1].size()<<endl<<flush;



//========================================================
//= STEP 0 : CREATE A CONTEXT ============================
//========================================================

	Glouglou::Context ctx = Glouglou::Context(vec2(400,300),3,3);

//========================================================
//= SETP 1 : MAKE A PROGRAM ON THE GPU ===================
//========================================================

	glDisable(GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// glBlendFunc(GL_ZERO, GL_SRC_COLOR);
	// glBlendFunc(GL_ONE, GL_ONE);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	Glouglou::GPU_Program shader;
		// shader.set_vertex_shader( file_to_str("shader/identity.vert"));
		// shader.set_fragment_shader( file_to_str("shader/noise.frag"));
		shader.set_vertex_shader( file_to_str("shader/laurent_data_zoom2.vert"));
		shader.set_fragment_shader( file_to_str("shader/color2.frag"));
		// shader.set_geometry_shader( file_to_str("shader/laurent_eyer_points.geom"));
		shader.compile();


//========================================================

	// vector<vec2> data_sigma_square;
	// vector<vec2> data_dispersion;

	// vector<int>		laurent_data_vector_CODE;
	// vector<int>		laurent_data_vector_NUIT;
	vector<float>	laurent_data_vector_PJ;
	vector<float>	laurent_data_vector_PJ_relative;
	vector<float>	laurent_data_vector_PJ_last;
	// vector<float>	laurent_data_vector_FZ;
	// vector<int>		laurent_data_vector_IPV;
	// vector<float>	laurent_data_vector_VM;
	// vector<int>		laurent_data_vector_IPC;
	vector<float>	laurent_data_vector_U;
	vector<float>	laurent_data_vector_V;
	// vector<float>	laurent_data_vector_B1;
	// vector<float>	laurent_data_vector_B2;
	// vector<float>	laurent_data_vector_V1;
	// vector<float>	laurent_data_vector_G;
	vector<vec3>	laurent_data_vector_color;
	vector<unsigned int> indices;

	size_t index_counter=0;
	for (size_t i=0;i<datas_split_by_id.size();i++) {
		vector<Data>& d = datas_split_by_id[i];


		// ranger dans l'ordre de nuit

		sort(d.begin(),d.end(),sort_by_PJ);
		for (size_t i = 0; i < d.size()-1; i++) {
			if( d[i].PJ > d[i+1].PJ) {
				cout<<"ERROR"<<endl;
			}
		}


		size_t index_counter_local=0;
		float x_min = std::numeric_limits<float>::max();
		float x_max = std::numeric_limits<float>::min();
		float y_min = std::numeric_limits<float>::max();
		float y_max = std::numeric_limits<float>::min();
		size_t i_max = d.size();
		float PJ_first = d[0].PJ;
		float PJ_last = d.back().PJ;
		vec3 col =  vec3(random(1.),random(1.),random(1.)) ;
		for (size_t i = 0; i < i_max; i++) {
			// enlever si IPV ou IPC == 0 1 ou 2
			if (true || d[i].IPV == 0 || d[i].IPV == 1 || d[i].IPV == 2 ||
				d[i].IPC == 0 || d[i].IPC == 1 || d[i].IPC == 2) {

				// laurent_data_vector_CODE.push_back( datas[i].CODE );
				// laurent_data_vector_NUIT.push_back( datas[i].NUIT );
				laurent_data_vector_PJ.push_back(            datas[i].PJ );
				laurent_data_vector_PJ_relative.push_back(   datas[i].PJ - PJ_first );
				laurent_data_vector_PJ_last.push_back(       PJ_last     );
				// laurent_data_vector_FZ.push_back(   datas[i].FZ   );
				// laurent_data_vector_IPV.push_back(  datas[i].IPV  );
				// laurent_data_vector_VM.push_back(   datas[i].VM   );
				// laurent_data_vector_IPC.push_back(  datas[i].IPC  );
				laurent_data_vector_U.push_back( -d[i].V + dither_noise(0.0005) );
				laurent_data_vector_V.push_back( -d[i].U + dither_noise(0.0005) );
				// laurent_data_vector_B1.push_back(   datas[i].B1   );
				// laurent_data_vector_B2.push_back(   datas[i].B2   );
				// laurent_data_vector_V1.push_back(   datas[i].V1   );
				// laurent_data_vector_G.push_back(    datas[i].G    );
				laurent_data_vector_color.push_back( col );
				index_counter_local++;
				if(d[i].U<x_min) {x_min = d[i].U;}
				if(d[i].U>x_max) {x_max = d[i].U;}
				if(d[i].V<y_min) {y_min = d[i].V;}
				if(d[i].V>y_max) {y_max = d[i].V;}
			}
		}
		// data_dispersion.push_back(vec2(x_min-x_max,y_min-y_max));
		if( index_counter_local>1 ) {
			for(size_t i=0;i < index_counter_local - 1 ;i++) {
				indices.push_back( index_counter + i );
				indices.push_back( index_counter + i+1 );
			}
		}
		index_counter+=index_counter_local;



	}

	//========================================================
	//= STEP 2 : SEND DATA TO THE GPU ========================
	//========================================================

	Glouglou::GPU_Data laurent_data_X;
	Glouglou::GPU_Data laurent_data_Y;
	Glouglou::GPU_Data laurent_data_color;
	Glouglou::GPU_Data laurent_data_PJ_relative;
	Glouglou::GPU_Data laurent_data_PJ_last;
	
	laurent_data_X.to_VRAM(laurent_data_vector_U,indices);
	laurent_data_Y.to_VRAM(laurent_data_vector_V,indices);
	laurent_data_color.to_VRAM(laurent_data_vector_color,indices);
	// laurent_data_PJ_relative.to_VRAM(laurent_data_vector_PJ_relative,incides);
	// laurent_data_PJ_last.to_VRAM(laurent_data_vector_PJ_last,incides);

	//========================================================
	//= STEP 3 : SETUP PROGRAM CALL (LINK DATA TO GPU INPUT) =
	//========================================================
	Glouglou::GPU_Program_Runner program_runner(shader);
		program_runner.link_data(laurent_data_X,"X");
		program_runner.link_data(laurent_data_Y,"Y");
		program_runner.link_data(laurent_data_color,"color");
		// program_runner.link_data(laurent_data_PJ_relative,"PJ");
		// program_runner.link_data(laurent_data_PJ_last,"PJ_last");


	cout<<flush;
	cout<<"star count : "<<datas_split_by_id.size()<<endl;
	cout<<flush;






//========================================================
//========================================================
//========================================================











	vec2 zoom;
	zoom.x=1;
	zoom.y=1;
	vec2 camera_position;
	camera_position.x=0;
	camera_position.y=0;

	bool quit=false;
	SDL_Event e;
	const Uint8 *keyboard_state;
	float t=0;
	float exposure =.2;
	float e_threshold_x=1;
	float e_threshold_y=1;
	bool e_fade=false;
	bool render_dots=true;
	bool render_lines=true;
	bool distinguish_color=false;
	while(!quit) {
		keyboard_state = SDL_GetKeyboardState(NULL);
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			if (e.type == SDL_MOUSEWHEEL) {
				if(keyboard_state[SDL_SCANCODE_LCTRL] ) {
					zoom.x *= pow(1.1,-e.wheel.x);
					zoom.y *= pow(1.1,e.wheel.y);
				} else if (keyboard_state[SDL_SCANCODE_LSHIFT]) {
					exposure += 0.01 * e.wheel.y;
					exposure = clamp(exposure , 0.01f , 1.f);
				} else if (keyboard_state[SDL_SCANCODE_E]) {
					e_threshold_x += 0.01*e.wheel.x/zoom.x;
					e_threshold_y +=-0.01*e.wheel.y/zoom.y;
				} else {
					camera_position.x +=-0.02*e.wheel.x/zoom.x;
					camera_position.y += 0.02*e.wheel.y/zoom.y;
				}
			}
			if (e.type == SDL_WINDOWEVENT) {
				if(e.window.event == SDL_WINDOWEVENT_RESIZED) {
					glViewport(0,0,e.window.data1,e.window.data2);
				}
			}
			if (e.type == SDL_KEYDOWN) {
				if (e.key.keysym.sym == SDLK_p) {
					render_dots = !render_dots;
				}
				if (e.key.keysym.sym == SDLK_l) {
					render_lines = !render_lines;
				}
				if (e.key.keysym.sym == SDLK_c) {
					distinguish_color = !distinguish_color;
				}
				if (e.key.keysym.sym == SDLK_e && keyboard_state[SDL_SCANCODE_LSHIFT]) {
					e_fade = !e_fade;
				}

			}
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// DRAW
		t+=0.001;

		{
			shader.use();
			shader.set_uniform("time",t);
			shader.set_uniform("zoom_factor",zoom);
			shader.set_uniform("camera_position",camera_position);
			shader.set_uniform("exposure",exposure);
			

			srand(3);

				// vec2 disp = data_dispersion[i];
				// float e = 1;
				// if (e_fade) {
				// 	// e = e_threshold_x*disp[0]*disp[0] + e_threshold_y*disp[1]*disp[1];
				// 	// e = e_threshold_x*disp[0]*disp[0];
				// 	e = e_threshold_x + disp[0]*disp[0];
				// 	if (e<e_threshold_y){e=0;}
				// 	else if (e>1){e=0;}
				// 	else {e=1;}
				// }
				// vec3 color = vec3(1,1,1);
				// if (distinguish_color) {
				// 	color = vec3(random(1.),random(1.),random(1.));
				// }
				if (render_lines) {
					program_runner.draw_edges();
				}
				if( render_dots ) {
					program_runner.draw_points();
				}
				// program_runner.draw_triangles();
		}

		ctx.swap();
	}
	return 0;
}
