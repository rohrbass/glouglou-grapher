#version 330

in float X;
in float Y;
in vec3 color;
in float PJ;
in float PJ_last;
uniform vec2 zoom_factor;
uniform vec2 camera_position;
uniform float time;
out vec4 v_pos;
out vec3 v_color;
out float v_PJ;
out float v_PJ_last;

void main() {
	gl_PointSize = max(1.0, max(zoom_factor.y/2,zoom_factor.x/2) );
	vec2 p = vec2(X,Y) - camera_position;
	v_color = color;
	v_PJ=PJ;
	v_PJ_last=PJ_last;

	p = zoom_factor * p;

	gl_Position = vec4(p,0,1);
	v_pos = gl_Position;

}
