#version 330

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform float geom_square_size;
uniform vec2 zoom_factor;
uniform vec2 resolution;

void main() {
	vec2 s = vec2(zoom_factor.x,zoom_factor.y)*geom_square_size;

	// todo clamp [s,t] to 1/resolution

	s = max(1/resolution,s);

	gl_Position = gl_in[0].gl_Position + vec4(-s.x, +s.y, 0.0, 0.0);
	EmitVertex();

	gl_Position = gl_in[0].gl_Position + vec4(-s.x, -s.y, 0.0, 0.0);
	EmitVertex();

	gl_Position = gl_in[0].gl_Position + vec4(+s.x, +s.y, 0.0, 0.0);
	EmitVertex();

	gl_Position = gl_in[0].gl_Position + vec4(+s.x, -s.y, 0.0, 0.0);
	EmitVertex();

	EndPrimitive();
}
