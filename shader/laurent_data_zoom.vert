#version 330

in float X;
in float Y;
uniform vec2 zoom_factor;
uniform vec2 camera_position;
out vec4 v_pos;

void main() {
	gl_PointSize = max(1.0, max(zoom_factor.y/2,zoom_factor.x/2) );
	vec2 p = vec2(X,Y) - camera_position;

	p = zoom_factor * p;

	gl_Position = vec4(p,0,1);
	v_pos = gl_Position;

}
