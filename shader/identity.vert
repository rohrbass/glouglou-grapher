#version 330

in vec4 position;
out vec4 v_pos;

void main() {
	gl_PointSize = 10.0;
	gl_Position = position;
	v_pos = position;
}
