#version 330

in vec3 v_color;
in float v_PJ;
in float v_PJ_last;
uniform float time;
uniform float exposure;
// out vec4 out_color;

void main() {

	// out_color = color;

	// t in [0,v_PJ_last]
	float t = mod(time,v_PJ_last);
	if(t<0) {t+=v_PJ_last;} // need to rethink this thing

	// need to rethink this thing
	// float t = time - v_PJ_last * floor (time/v_PJ_last);
	// if(t<0) {t+=v_PJ_last;}
	// float t = time; // t has to be in [0,v_PJ]
	// while (t > v_PJ_last) {t -= v_PJ_last;}
	// while (t < 0) {t += v_PJ_last;}


	float t_distance = pow(t-v_PJ,4);
	float trail = 1-max(0,t_distance)*0.00000001;
	// gl_FragColor = vec4(v_color,exposure);
	// gl_FragColor = vec4(v_color,trail*exposure);
	gl_FragColor = vec4(v_color,trail);
}
