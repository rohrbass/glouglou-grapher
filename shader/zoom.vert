#version 330

in vec4 position;
in float temperature;
uniform vec2 zoom_factor;
uniform vec2 camera_position;
out vec4 v_pos;

void main() {
	gl_PointSize = 10.0;
	vec2 p = position.xy - camera_position;

	p = zoom_factor * p;

	gl_Position = vec4(p,0,1);
	v_pos = gl_Position;

}
