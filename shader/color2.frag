#version 330

in vec3 v_color;
uniform float time;
uniform float exposure;
// out vec4 out_color;

void main() {

	// out_color = color;
	gl_FragColor = vec4(v_color,exposure);
}
